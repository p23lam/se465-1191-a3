#!/usr/bin/env bash
case $(id -u) in
    0)
        apt-get update
        apt-get install -y default-jdk maven git g++ make valgrind

        sudo -u vagrant -i $0
        ;;
    *)
        ;;
esac
